
# TN Event - Scanner Twitch

Ce service sert à faire le lien entre le TN Event et Twitch. Il permet de récupérer l'état des streamers en temps réel et de les distribuer vers la notification-api.

TODO: lien vers l'api

## Dépendance

 - github.com/joho/godotenv v1.4.0 : Gestion de l'environnement en dev
 - github.com/rabbitmq/amqp091-go v1.5.0 : Gestion du RabbitMQ
 - github.com/nicklaw5/helix v1.25.0 : Wrapper de l'API Twitch

## Environnement 

- `RBMQ_CONNECTION_STRING` : Permet la connection au service RabbitMQ
- `RBMQ_EXCHANGE_NAME` : Nom de l'échange où transite les données
- `API_STREAMERS` : Lien qui retourne une liste de streamers
- `API_UPDATE_INTERVAL` : Interval de mise à jour des streamers (faible en dev, élevé en prod)
- `TWITCH_CLIENT_ID` : Client ID pour l'[API Twitch](https://dev.twitch.tv/console.apps/create)
- `TWITCH_CLIENT_SECRET` : Client Secret pour l'API Twitch
- `DEBUG` : Affiche quelques messages dans les logs
- `DEBUG_EXTENDED` : Affiche absolument tout
- `MODE` : Détermine le mode de l'application `prod` ou `dev`

## Lancement en dev

Il faut d'abord s'assurer d'avoir un .env **complet** et [Go](https://go.dev/) d'installer.

```
git clone **URL**
cd twitch-scanner
go run cmd/server/main.go
```

## Déploiement (prod)

TODO

## Status

Le service ouvre un port de metrics qui doit rester **fermer** (ou sous mot de passe) en production qui est le port `2112`. Il permet de vérifier le status de l'application sur `/health` et de vérifier la consommation du service sur `/debug/pprof`