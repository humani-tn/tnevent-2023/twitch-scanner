package streamers

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"os"
)

// Streamer is a struct that represents a streamer from the API
type Streamer struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	TwitchLogin string `json:"twitch_id"`
	ID          int    `json:"id"`
	TeamID      string `json:"team_id"`
	ImageFile   string `json:"image_file"`

	LastViewersCount int
	LastViewers      time.Time
	LastLive         bool
	LastCategory     string
}

// GetTopic returns the topic for the streamer, used for RabbitMQ
func (s *Streamer) GetTopic() string {
	// return "streamers." + strconv.Itoa(s.ID) + ".live"
	return fmt.Sprintf("streamers.%v.live", s.ID)
}

// GetStreamers returns a list of streamers from the API
func GetStreamers() []*Streamer {
	URL := os.Getenv("API_STREAMERS")
	if URL == "" {
		panic("API_STREAMERS is not set")
	}

	// Prepare the request
	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		panic(err)
	}

	// Send the request
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}

	// Parse the response here
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	// Return the streamers
	var streamers []*Streamer
	json.Unmarshal(body, &streamers)

	return streamers
}
