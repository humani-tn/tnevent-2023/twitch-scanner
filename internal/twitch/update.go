package twitch

import (
	"fmt"
	"log"
	"os"
	"time"
	"twitch-scanner/internal/rbmq"

	"github.com/nicklaw5/helix"
)

// We also need to keep the last state of each streamer to be able to send a message only when it changes (caching baby)
var lastLiveState = make(map[string]bool)

var client *helix.Client

// GetClient returns a Twitch client, it uses helix which is a Go wrapper for the Twitch API
func GetClient() *helix.Client {
	// Check that env variables are set
	if os.Getenv("TWITCH_CLIENT_ID") == "" || os.Getenv("TWITCH_CLIENT_SECRET") == "" {
		panic("TWITCH_CLIENT_ID and TWITCH_CLIENT_SECRET must be set")
	}
	// Create a new client
	client, err := helix.NewClient(&helix.Options{
		ClientID:     os.Getenv("TWITCH_CLIENT_ID"),
		ClientSecret: os.Getenv("TWITCH_CLIENT_SECRET"),
	})
	if err != nil {
		panic(err)
	}
	// Get an app access token
	resp, err := client.RequestAppAccessToken([]string{})
	if err != nil {
		panic(err)
	}
	// Set the app access token
	client.SetAppAccessToken(resp.Data.AccessToken)
	return client
}

func update() {
	for _, streamer := range streamerMap {
		if time.Since(streamer.LastViewers).Seconds() > 60 && streamer.LastLive {
			streamer.LastLive = false

			// The guy's offline
			data := map[string]interface{}{
				"d": map[string]string{
					"viewCount": "0",
					"isLive":    "false",
				},
				"r": streamer.GetTopic(),
			}

			// Send message to RabbitMQ
			err := rbmq.SendMessage(data, streamer.GetTopic())
			if err != nil {
				log.Printf("Error while sending message to RabbitMQ: %v", err)
			}
		} else {
			// Get the streamer's category
			streams, err := client.GetStreams(&helix.StreamsParams{
				UserLogins: []string{streamer.TwitchLogin},
			})
			if err != nil {
				log.Printf("Error while getting streams: %v", err)
				continue
			}

			var gameName string
			if len(streams.Data.Streams) > 0 {
				gameName = streams.Data.Streams[0].GameName
			}

			streamer.LastCategory = gameName

			data := map[string]interface{}{
				"d": map[string]string{
					"viewCount": fmt.Sprint(streamer.LastViewersCount),
					"isLive":    "true",
					"category":  gameName,
				},
				"r": streamer.GetTopic(),
			}

			// Send message to RabbitMQ
			err = rbmq.SendMessage(data, streamer.GetTopic())
			if err != nil {
				log.Printf("Error while sending message to RabbitMQ: %v", err)
			}
		}
	}
}
