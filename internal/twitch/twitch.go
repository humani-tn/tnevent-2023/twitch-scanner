package twitch

import (
	"fmt"
	"log"
	"os"
	"time"
	"twitch-scanner/internal/streamers"

	"github.com/gorilla/websocket"
)

var (
	// DefaultHost is the default host to connect to Twitch's pubsub servers
	DefaultHost = "wss://pubsub-edge.twitch.tv"
)

// We have to keep the streamers list in memory to be able to update it
var streamerMap = make(map[string]*streamers.Streamer)

// We also store the streamers list in a variable to be able to update it
var streamerList []*streamers.Streamer

// We need 10 pubsub clients to avoid rate limiting
var pubsubClients = make([]*websocket.Conn, 10)

func streamerClient(login string) *websocket.Conn {
	// Get the last digit of the streamer ID
	lastDigit := login[len(login)-1:]
	// Convert it to an integer
	lastDigitInt := int(lastDigit[0]-'a') % 10
	// Get the client
	client := pubsubClients[lastDigitInt]

	// If the client is disconnected, reconnect
	if client == nil {
		// Create a new client
		conn, _, err := websocket.DefaultDialer.Dial(DefaultHost, nil)
		if err != nil {
			panic(err)
		}
		// Set the client
		pubsubClients[lastDigitInt] = conn
	}

	return client
}

func doUpdate() {
	if os.Getenv("DEBUG") == "true" {
		log.Printf("Updating streamers list.")
	}

	streamerList = streamers.GetStreamers()

	// fmt.Println(streamerList)

	for _, streamer := range streamerList {
		streamerMap[streamer.TwitchLogin] = streamer

		// Subscribe to the streamer's channel
		topic := fmt.Sprintf("video-playback.%v", streamer.TwitchLogin)

		if os.Getenv("DEBUG") == "true" {
			log.Printf("Subscribing to %v", topic)
		}

		streamerClient(streamer.TwitchLogin).WriteMessage(websocket.TextMessage, []byte(`{"type":"LISTEN","data":{"topics":["`+topic+`"]}}`))

		time.Sleep(100 * time.Millisecond)
	}
}

// autoUpdate updates the streamers list every API_UPDATE_INTERVAL (this is a random value, it can be changed)
func autoUpdate() {
	// Get the update interval from the environment variables
	dt := os.Getenv("API_UPDATE_INTERVAL")
	if dt == "" {
		dt = "30m"
	}
	// Parse the duration
	duration, err := time.ParseDuration(dt)
	if err != nil {
		panic(err)
	}

	doUpdate()

	// Create a ticker that will update the streamers list every duration
	t := time.NewTicker(duration)
	for range t.C {
		doUpdate()
	}
}

func Startup() {
	if os.Getenv("DEBUG") == "true" {
		log.Printf("Loaded queues and streamer map.")
	}

	client = GetClient()

	// Update streamers list every 30 minutes
	go autoUpdate()

	// Main loop that updates the streamers every 5 seconds
	// With a ratelimit of 800 points per minute, and an update making 1 request for every 100 streamers
	// We can theorically update 6600 streamers per minute
	t := time.NewTicker(10 * time.Second)
	for range t.C {
		update()
	}
}
