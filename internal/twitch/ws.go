package twitch

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"
	"twitch-scanner/internal/rbmq"

	"github.com/gorilla/websocket"
)

type PubSubMessage struct {
	Type string `json:"type"`
	Data struct {
		Topic   string `json:"topic"`
		Message string `json:"message"`
	} `json:"data"`
}

type PubSubMessageData struct {
	Type       string  `json:"type"`
	ServerTime float64 `json:"server_time"`
	Viewers    int     `json:"viewers"`
}

func wsPing(conn *websocket.Conn) {
	for {
		time.Sleep(60 * time.Second)
		conn.WriteMessage(websocket.TextMessage, []byte(`{"type":"PING"}`))
	}
}

func wsRead(conn *websocket.Conn, id int) {
	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			break
		}

		if !strings.Contains(string(message), "video-playback") {
			continue
		}

		var msg PubSubMessage
		err = json.Unmarshal(message, &msg)
		if err != nil {
			log.Println("unmarshal:", err)
			continue
		}

		var msgData PubSubMessageData
		err = json.Unmarshal([]byte(msg.Data.Message), &msgData)
		if err != nil {
			log.Println("unmarshal:", err)
			continue
		}

		// Get the streamer
		streamer, ok := streamerMap[strings.Split(msg.Data.Topic, ".")[1]]
		if !ok {
			continue
		}
		streamer.LastViewers = time.Now()
		streamer.LastLive = true

		// Prepare message
		data := map[string]interface{}{
			"d": map[string]string{
				"streamerID": fmt.Sprint(streamer.ID),
				"viewCount":  fmt.Sprintf("%v", msgData.Viewers),
				"isLive":     "true",
				"category":   streamer.LastCategory,
				"timestamp":  fmt.Sprint(streamer.LastViewers.Unix()),
			},
			"r": streamer.GetTopic(),
		}

		if msgData.Viewers == 0 && streamer.LastViewersCount != 0 {
			streamer.LastViewersCount = msgData.Viewers
			continue
		}

		// Send message to RabbitMQ
		err = rbmq.SendMessage(data, streamer.GetTopic())
		if err != nil {
			log.Printf("Error while sending message to RabbitMQ: %v", err)
		}

		streamer.LastViewersCount = msgData.Viewers
	}

	// Create a new client
	conn, _, err := websocket.DefaultDialer.Dial(DefaultHost, nil)
	if err != nil {
		panic(err)
	}
	pubsubClients[id] = conn
	go wsPing(conn)
	go wsRead(conn, id)
}

func init() {
	// Initialize the pubsub clients
	for i := 0; i < 10; i++ {
		// Create a new client
		conn, _, err := websocket.DefaultDialer.Dial(DefaultHost, nil)
		if err != nil {
			panic(err)
		}

		pubsubClients[i] = conn

		// Send PING messages every minute
		go wsPing(conn)

		// Read messages
		go wsRead(conn, i)
	}
}
