package rbmq

import (
	"log"
	"os"
	"time"
)

// connectionWatchdog is a goroutine that checks every second if the connection is closed
// If it is, it tries to reconnect
func connectionWatchdog() {
	// Ticker that checks every seconds the connection status
	t := time.NewTicker(1 * time.Second)
	for {
		select {
		// If the ticker ticks
		case <-t.C:
			// If the connection is closed, try to reconnect
			// This is done to avoid a panic when the connection is lost
			if connection == nil || channel == nil {
				if os.Getenv("DEBUG") == "true" {
					log.Println("Trying to reconnect to RabbitMQ...")
				}
				connection, channel = connect()
			} else if connection.IsClosed() {
				if os.Getenv("DEBUG") == "true" {
					log.Println("Trying to reconnect to RabbitMQ...")
				}
				connection, channel = connect()
			}
		}
	}
}
