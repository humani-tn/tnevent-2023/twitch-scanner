package rbmq

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"os"

	amqp "github.com/rabbitmq/amqp091-go"
)

// SendMessage sends a message to the exchange
func SendMessage(message map[string]interface{}, key string) error {
	body, err := json.Marshal(message)
	if err != nil {
		return err
	}

	if os.Getenv("DEBUG_EXTENDED") == "true" {
		log.Printf("Sending message to queue: %s\n", string(body))
	}

	if channel == nil {
		return errors.New("not connected to RabbitMQ")
	}

	return channel.PublishWithContext(
		context.Background(),
		os.Getenv("RBMQ_EXCHANGE_NAME"),
		key,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		},
	)
}
