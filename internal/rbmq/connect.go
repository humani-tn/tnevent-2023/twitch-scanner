// Package rbmq provides a connection to RabbitMQ
// It also provides a function to get the channel
// And renew the connection if it's lost
package rbmq

import (
	"log"
	"os"

	amqp "github.com/rabbitmq/amqp091-go"
)

// We need to keep the connection and channel in memory to be able to reconnect
var channel *amqp.Channel
var connection *amqp.Connection

func Startup() {
	// Verify that the environment variables are set
	if os.Getenv("RBMQ_CONNECTION_STRING") == "" {
		log.Fatal("RBMQ_CONNECTION_STRING is not set")
	}
	if os.Getenv("RBMQ_EXCHANGE_NAME") == "" {
		log.Fatal("RBMQ_EXCHANGE_NAME is not set")
	}

	// Connect to RabbitMQ for the first time
	conn, ch := connect()
	channel = ch
	connection = conn

	// Start the watchdog
	go connectionWatchdog()
}

func connect() (conn *amqp.Connection, ch *amqp.Channel) {
	connectionString := os.Getenv("RBMQ_CONNECTION_STRING")

	// Connects to RabbitMQ
	conn, err := amqp.Dial(connectionString)
	if err != nil {
		if os.Getenv("DEBUG") == "true" {
			log.Printf("Failed to connect to RabbitMQ: %s\n", err)
		}
		return
	}

	// Creates a channel to communicate with RabbitMQ
	ch, err = conn.Channel()
	if err != nil {
		if os.Getenv("DEBUG") == "true" {
			log.Printf("Failed to connect to RabbitMQ: %s\n", err)
		}
		return
	}

	if os.Getenv("DEBUG") == "true" {
		log.Println("Connected to RabbitMQ")
	}

	return
}
