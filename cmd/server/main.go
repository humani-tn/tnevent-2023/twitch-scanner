// Package main is the entry point for the server
// It will start the RabbitMQ connection and the Twitch connection
// It's also responsible for updating the streamers list for the first time
package main

import (
	"log"
	"net/http"
	"net/http/pprof"
	"os"
	"twitch-scanner/internal/rbmq"
	"twitch-scanner/internal/twitch"

	"github.com/joho/godotenv"

	_ "golang.org/x/crypto/x509roots/fallback" // CA bundle for docker image
)

func main() {
	// Load environment variables from .env when in development
	err := godotenv.Load(".env")
	if err != nil && os.Getenv("MODE") != "prod" {
		log.Panicf("Error loading .env file: %s\nIf this is in production, don't forget MODE=prod !", err)
	}

	// Start RabbitMQ connection
	go rbmq.Startup()

	// Start the metrics server and make routes manually
	mux := http.NewServeMux()
	mux.HandleFunc("/debug/pprof/", pprof.Index)
	mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	mux.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
	mux.Handle("/debug/pprof/heap", pprof.Handler("heap"))
	mux.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))
	mux.Handle("/debug/pprof/block", pprof.Handler("block"))

	// Add a health check route
	mux.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	// Start the metrics server on port 2112
	go http.ListenAndServe(":2112", mux)

	// Start Twitch connection (and locks the main thread)
	twitch.Startup()
}
