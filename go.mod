module twitch-scanner

go 1.19

require (
	github.com/gorilla/websocket v1.5.0
	github.com/joho/godotenv v1.4.0
	github.com/nicklaw5/helix v1.25.0
	github.com/rabbitmq/amqp091-go v1.5.0
	golang.org/x/crypto/x509roots/fallback v0.0.0-20250118192723-a8ea4be81f07
)

require github.com/golang-jwt/jwt v3.2.1+incompatible // indirect
